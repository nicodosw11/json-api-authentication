**Scenario** 

User registration JSON API and authentication system (REST-ful API JSON responses only)

The app is able do the following:

- Register with a username, email address and password
```bash
curl -X POST -H "Content-Type: application/json" http://localhost:3000/api/v1/signup -d '{"user": {"username":"johndoe", "email":"johndoe@myemail.com", "password":"abcd1234"}}'
# {"id":22,"username":"johndoe","email":"johndoe@myemail.com","password_digest":"$2a$10$d8UJDxM/FJdh1l8xjFpDteI4scGjYVdjWDQt5A9Z5QBQ.uDq2WSPO","admin":false,"created_at":"2019-02-03T21:45:02.782Z","updated_at":"2019-02-03T21:45:02.782Z"}
```
- Authenticate with a username and password
```bash
curl -v --data "auth[username]=johndoe&auth[password]=abcd1234" http://localhost:3000/api/v1/signin 
# {"jwt":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NDkyMzM5MzIsInN1YiI6MjJ9.Cszl_2p-3Tx_uGCpLfsb09DRSWB05UlRL_D2vikM2Rw"}
```
- Request the user’s details (username, email address and group memberships) with a valid user token and providing a username or email address
```ruby
curl -v -X GET -H "Authorization: JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NDkyMzM5MzIsInN1YiI6MjJ9.Cszl_2p-3Tx_uGCpLfsb09DRSWB05UlRL_D2vikM2Rw" http://localhost:3000/api/v1/profile -d '{"username":"mayola", "email":"melva@spencer.io"}'
curl -v -X GET -H "Authorization: JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NDkyMzM5MzIsInN1YiI6MjJ9.Cszl_2p-3Tx_uGCpLfsb09DRSWB05UlRL_D2vikM2Rw" http://localhost:3000/api/v1/profile -d '{"username":"mayola"}'
curl -v -X GET -H "Authorization: JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NDkyMzM5MzIsInN1YiI6MjJ9.Cszl_2p-3Tx_uGCpLfsb09DRSWB05UlRL_D2vikM2Rw" http://localhost:3000/api/v1/profile -d '{"email":"melva@spencer.io"}'
# {"user":{"username":"mayola","email":"melva@spencer.io","memberships":[{"id":2,"name":"football aficionados","created_at":"2019-02-03T20:05:11.133Z","updated_at":"2019-02-03T20:05:11.133Z"}]}
```
- Assign users to groups
```ruby
curl -X PATCH -H  "Authorization: JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NDkyMzM5MzIsInN1YiI6MjJ9.Cszl_2p-3Tx_uGCpLfsb09DRSWB05UlRL_D2vikM2Rw" http://localhost:3000/api/v1/groups/1/add_user -d '{"email":"blanch@kuphal.org"}'
# {"status":200,"msg":"cliff_krajcik has joined outdooraholics"}% 
curl -X PATCH -H  "Authorization: JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NDkyMzM5MzIsInN1YiI6MjJ9.Cszl_2p-3Tx_uGCpLfsb09DRSWB05UlRL_D2vikM2Rw" http://localhost:3000/api/v1/groups/1/add_user -d '{"username":"cliff_krajcik"}'
{"msg":"cliff_krajcik is already a member"}%  
# {"msg":"cliff_krajcik is already a member"}
```



