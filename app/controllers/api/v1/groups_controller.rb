class Api::V1::GroupsController < ApplicationController
  before_action :set_group, only: [:show, :add_user, :update, :destroy]
  before_action :authenticate_user
  before_action :authenticate_admin,  only: [:create, :add_user]

  # GET /groups
  def index
    @groups = Group.all

    render json: @groups
  end

  # GET /groups/1
  def show
    render json: @group
  end

  # POST /groups
  def create
    @group = Group.new(group_params)

    if @group.save
      render json: @group, status: :created
    else
      render json: @group.errors, status: :unprocessable_entity
    end
  end

  def add_user
    @user = User.by_username_or_email(username: params[:username], email: params[:email])
    if !@group.users.include? @user
      @group.users << @user
      render json: {status: 200, msg: "#{@user.username} has joined #{@group.name}"}
    else
      render json: {msg: "#{@user.username} is already a member"}
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_group
      @group = Group.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def group_params
      params.require(:group).permit(:name)
    end
end
