class ApplicationController < ActionController::API
  include Knock::Authenticable

  protected

  def authenticate_admin
    return_unauthorized unless !current_user.nil? && current_user.admin?
  end
end
