class User < ApplicationRecord
  has_secure_password
  has_many :memberships
  has_many :groups, through: :memberships
  validates :username, presence: true, uniqueness: true
  validates_presence_of :email
  validates_uniqueness_of :email
  validates_format_of :email, with: /@/
  validates :password, length: { in: 6..20 }

  def self.by_username_or_email(username:, email:)
    User.where('username = ? OR email = ?', username, email).first
  end

  def self.from_token_request request
    username = request.params["auth"] && request.params["auth"]["username"]
    self.find_by username: username
  end
end
