Rails.application.routes.draw do
  namespace :api, {format: 'json'} do
    namespace :v1 do
      post 'signin' => "user_token#create"
      post 'signup' => "users#create"
      resources :groups, only: [:index, :show, :create]
      patch '/groups/:id/add_user' => "groups#add_user"
      resources :users, only: [:index]
      get '/profile' => "users#profile"
    end
  end
end
