# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'faker'

User.destroy_all
Group.destroy_all

groups = %w[outdooraholics football\ aficionados london\ explorers elite\ socialites cheese\ lovers]
groups.each { |group| Group.create!(name: group) }

p "#{Group.all.count} groups created"
Group.all.each { |c| puts "#{c.name}" }

User.create!(username: "Jack",
             email: "jack.sparrow@example.com",
             password: "secretstuff",
             admin: true)

20.times do |i|
  puts "Creating User ##{i+1}"
  User.create!(
    username: Faker::Internet.unique.user_name,
    email: Faker::Internet.unique.email,
    # password: Faker::Internet.password(8),
    password: "secretstuff",
  )
end

p "#{User.all.count} users created"
User.all.each { |c| puts "#{c.username} - #{c.email}" }

Membership.destroy_all
20.times do |i|
  Group.find(Group.ids.shuffle.first).users << User.all.sample(1)
end

p "#{Membership.all.count} memberships created"
Membership.all.each { |m| puts "#{m.user.username} joined #{m.group.name}"}
